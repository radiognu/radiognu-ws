# Servidor WebSockets para RadioÑú

Este script de node.js implementa un servidor para el protocolo WebSockets que
devuelve el siguiente JSON:

```json
{ "title": "Título", "artist": "Artista" }
```
La cual contiene los metadatos básicos para poder llamar a la
[API](https://gitlab.com/radiognu/radiognu-api). En un futuro cercano, este
proyecto derivará en una implementación de la API usando WebSockets.

## ¿Cómo conectarse? ##

Usando cualquier cliente compatible con [`socket.io`](http://socket.io/), por
ejemplo, con JavaScript:

```html
<script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
<script>
    var socket = io('wss://radiognu.org:7777');
    socket.on('metadata', function(data) {
        console.log('Recibidos nuevos metadatos: ' + JSON.stringify(data));
    });
    socket.on('listeners', function(listeners) {
        console.log('Recibidos datos de escuchas conectados: ' + listeners);
    });
</script>
```

## ¿Cómo funciona? ##

:warning: Detalles técnicos a continuación.

El servidor mantiene ejecutando una instancia de `socket.io` gracias a
[`forever`](https://www.npmjs.com/package/forever), el cual escucha los cambios
del archivo `now_playing.json` que contiene los metadatos de la canción en
reproducción actualmente y que es reescrito por el script liquidsoap que
mantiene el servidor de flujos de audio de la radio. Cada vez que el archivo
cambia, se verifica que los datos no sean idénticos a los que ya hay (debido a
que el evento `on_metadata` de liquidsoap se dispara varias veces y si son
distintos se emite el evento `newdata` de socket.io con los metadatos.

Para la versión con SSL, se crea un servidor usando express, que facilita la
inclusión de los certificados, los cuales han sido creados usando
[Let's Encrypt](https://letsencrypt.org/).

## ¿Cómo ejecutar? ##

Para instalar las dependencias necesarias, se ejecuta `npm install` y para
ejecutar el servidor:

```bash
$ npm run ws
```

Para la versión con SSL, se requieren los archivos `privkey.pem` y `cert.pem`
que son generados por Let's Encrypt y ejecutar:

```bash
$ npm run wss
```

También existen los comandos `npm run stop_ws[s]`, `npm run restart_ws[s]`,
`npm run stopall` y `npm run restartall` que detienen y reinician los servicios.
