/*
 * app_wss.js -- Servidor WebSockets con SSL para RadioÑú
 *
 * Copyright 2017 Felipe Peñailillo <breadmaker@radiognu.org>
 *
 * Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
 * los términos de la Licencia Pública General GNU tal como se publica por
 * la Free Software Foundation; ya sea la versión 3 de la Licencia, o
 * (a su elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que le sea útil, pero SIN
 * NINGUNA GARANTÍA; sin incluso la garantía implícita de MERCANTILIDAD o
 * IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Vea la Licencia Pública
 * General de GNU para más detalles.
 *
 * Debería haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa; de lo contrario escriba a la Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, EE. UU.
 */

var fs = require('fs');
var app = require('express')();
var https = require('https');
var Tail = require('tail').Tail;

var server = https.createServer({
    key: fs.readFileSync('privkey.pem'),
    cert: fs.readFileSync('cert.pem'),
}, app);

server.listen(7777);

var io = require('socket.io').listen(server);

io.on('connection', function (socket) {
    var jsonfile = 'now_playing.json';
    socket.join('radiognu');
    var jsonContents = JSON.parse(fs.readFileSync(jsonfile));
    socket.emit('metadata', jsonContents);
    socket.emit('listeners', fs.readFileSync('listeners.txt').toString());
    fs.watch(jsonfile, function () {
        if (jsonContents != JSON.parse(fs.readFileSync(jsonfile))) {
            jsonContents = JSON.parse(fs.readFileSync(jsonfile));
            socket.emit('metadata', jsonContents);
        }
    });
    var tail = new Tail("icecast.log");
    tail.on("line", function (data) {
        if (data.indexOf("global listeners") !== -1) {
            var listeners = data.match(/\(([^)]+)\)/)[1];
            socket.emit('listeners', listeners);
            fs.writeFile('listeners.txt', listeners, function (err) {
                if (err) {
                    return console.log(err);
                }
            });
        }
    });
    tail.on("error", function (error) {
        console.log('ERROR: ', error);
    });
});
